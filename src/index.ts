import axios from "axios";

export class EtherscanClient {
    etherscanAPIkey: string;

    constructor(
        etherscanAPIkey: string,) {
        this.etherscanAPIkey = etherscanAPIkey
    }

    async isOwnerShipOpenSeaNFT(walletAddress: string, contractAddress: string, tokenID: string) {

        // https://docs.etherscan.io/api-endpoints/accounts#get-a-list-of-erc721-token-transfer-events-by-address
        const apiResponce = await axios.get('https://api.etherscan.io/api', {
            params: {
                module: 'account',
                action: 'tokennfttx',
                address: walletAddress,
                contractaddress: contractAddress,
                page: 1,
                offset: 100,
                startblock: 0,
                endblock: 27025780,
                sort: 'desc',
                apikey: this.etherscanAPIkey,
            }
        });
        var tokenTransferEvents: any[] = apiResponce.data.result;

        // OpenSeaは一つのコントラクトアドレスを共有してるため該当するNFTのtokenIDも見て判定
        const targetTokenLastEvent = tokenTransferEvents.find(event => event.tokenID == tokenID);
        if (targetTokenLastEvent == undefined) {
            return false
        }

        // 該当するトークンの取り引きが購入（toが自分のアドレス）かどうかで所持の判定
        if (targetTokenLastEvent.to.toUpperCase() == walletAddress.toUpperCase()) {
            return true
        }
        return false
    }




}




